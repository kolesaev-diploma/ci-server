# CI

## Подготовка CI
Для подготовки CI сервера выполните следующие команды:
   ```shell
   chmod +x ./CI-Server.sh #(только перед первым запуском)
   ./CI-Server.sh -a "create" -p "<your project id>" -c "<path to credentials file>" -r "<allowed source ip range>"
   ```

## Удаление CI
Для удаления CI сервера выполните следующую команду:
   ```shell
   chmod +x ./CI-Server.sh #(только перед первым запуском)
   ./CI-Server.sh -a "destroy" -p "<your project id>" -c "<path to credentials file>" -r "<allowed source ip range>"
   ```
   