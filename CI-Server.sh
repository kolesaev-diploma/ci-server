#!/bin/bash
cd $(dirname $(realpath $0))/infra

while getopts ":p:c:a:r:" flag;
do
    case "${flag}" in
        c) cred=${OPTARG};;
        p) id=${OPTARG};;
        a) action=${OPTARG};;
        r) sranges=${OPTARG};;
        *) x=${OPTARG};;
    esac
done;

# указываем файл с правами для terraform backend
terraform init -backend-config "credentials=$cred" -migrate-state;
# применяем конфиг terraform с учётом новых прав
#terraform init -migrate-state;

if [[ $action == "create" ]] ; then
    terraform apply --auto-approve -var "project-id=$id" -var "credentials-path=$cred" -var "source-ranges=$sranges"
fi;

if [[ $action == "destroy" ]] ; then
    terraform destroy --auto-approve -var "project-id=$id" -var "credentials-path=$cred" -var "source-ranges=$sranges"
fi;
