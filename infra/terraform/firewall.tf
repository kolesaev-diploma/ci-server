
# Получаем приватный ssh-ключ для инстанса
data "google_storage_bucket_object_content" "private-key" {
  name   = "ci-server"
  bucket = "task2-secrets"
}

# Разрешаем TCP и ICMP трафик к нашим инстансам
resource "google_compute_firewall" "default" {
  name          = "fw-task2-tcp"
  direction     = "INGRESS"
  network       = google_compute_network.default.id
  source_ranges = ["${var.source-ranges}"]
  allow {
    protocol = "icmp"
  }
  allow {
    protocol = "tcp"
    ports    = ["80","443","22","223"]
  }
  target_tags = ["instance"]

# копируем файлы ansible
  provisioner "file" {
    source      = "ansible/"
    destination = "/home/user"
    # указываем способ подключения
    connection {
      host        = "ci.kolesaevdevopst1.tk"
      type        = "ssh"
      user        = "user"
      private_key = "${data.google_storage_bucket_object_content.private-key.content}"      
    }
  }
  
# устанавливаем ansible и обновляем порт ssh
  provisioner "remote-exec" {
    inline = [
      "sudo apt update && sudo apt install ansible -y",
      "ansible-galaxy install geerlingguy.docker", 
      "ansible-playbook ~/gitlab/update-ssh.yaml", 
      "sleep 2 && sudo service sshd restart"
    ]
    # указываем способ подключения
    connection {
      host        = "ci.kolesaevdevopst1.tk"
      type        = "ssh"
      user        = "user"
      private_key = "${data.google_storage_bucket_object_content.private-key.content}"
    }
  }
}
