
# Подключаем дополнительный диск и запускаем деплой gitlab
resource "google_compute_attached_disk" "default" {
  disk     = "gitlab-home"
  instance = google_compute_instance.default.id
  depends_on = [google_compute_firewall.default]
  
# запускаем плейбук деплоя
  provisioner "remote-exec" {
    inline = [
      "ansible-playbook ~/gitlab/deploy.yaml"
    ]
    # указываем способ подключения (уже новый порт 223)
    connection {
      host        = "ci.kolesaevdevopst1.tk"
      type        = "ssh"
      port        = "223"
      user        = "user"
      private_key = "${data.google_storage_bucket_object_content.private-key.content}"
    }
  }
}
