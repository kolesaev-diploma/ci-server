
# Указываем, что мы хотим разворачивать окружение в GCP и указываем файл авторизации
provider "google" {
  project       = var.project-id
  region        = "europe-north1"
  zone          = "europe-north1-b"
  credentials  = "${file("${var.credentials-path}")}"
}
