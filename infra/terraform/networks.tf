
# Создвём внешний IP
resource "google_compute_address" "default" {
  name   = "ip-task2"
  region = "europe-north1"
}


# Прописываем IP-адрес инстанса нашему домену
resource "google_dns_record_set" "main_domain_ip" {
  name         = "ci.kolesaevdevopst1.tk."
  managed_zone  = "zone1"
  type         = "A"
  ttl          = 300
  rrdatas      = [google_compute_address.default.address]
}


# Создвём сеть
resource "google_compute_network" "default" {
  name                    = "network-task2"
  auto_create_subnetworks = false
}

# Создаём подсеть для наших инстансов
resource "google_compute_subnetwork" "default" {
  name          = "subnet-task2"
  ip_cidr_range = "10.0.2.0/24"
  network       = google_compute_network.default.id
}
