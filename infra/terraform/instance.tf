
# Получаем публичный ssh-ключ для инстанса
data "google_storage_bucket_object_content" "public-key" {
  name   = "ci-server.pub"
  bucket = "task2-secrets"
}

# Назначаем дистрибутив для запуска инстанса
data "google_compute_image" "ubuntu" {
  family  = "ubuntu-2004-lts"
  project = "ubuntu-os-cloud"
}

# Назначаем шаблон для инстансов
resource "google_compute_instance" "default" {
  name         = "instance-task2"
  machine_type = "e2-standard-2"
  tags         = ["instance"]

  lifecycle {
    create_before_destroy = true
    ignore_changes = [attached_disk]
  }
  network_interface {
    network    = google_compute_network.default.id
    subnetwork = google_compute_subnetwork.default.id
    access_config {
      nat_ip = google_compute_address.default.address
    }
  }
  # назначаем выбранный дистрибутив
  boot_disk {
    initialize_params {
      image = data.google_compute_image.ubuntu.self_link
    }
  }
  # назначаем ssh-ключи
  metadata = {
    ssh-keys = "user:${data.google_storage_bucket_object_content.public-key.content}"
  }
}
