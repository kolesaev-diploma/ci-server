# Назначаем bucket для tfstate
terraform {
  backend "gcs" {
    bucket  = "task2-tfstate"
    prefix  = "tfstate"
    region  = "europe-north1"
  }
}

# Создаём переменные
variable "project-id" {
  type = string
}
variable "credentials-path" {
  type = string
}
variable "source-ranges" {
  type = string
}

# подключаем написанный модуль из дочерней папки terraform-infra
module "infra" {
  source = "./terraform"
  # пробрасываем переменные в модуль
  project-id = var.project-id
  credentials-path = var.credentials-path
  source-ranges = var.source-ranges
}